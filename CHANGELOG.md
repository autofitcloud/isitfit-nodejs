Version latest (0.1.2?, 2019-09-05?)

- ...


Version 0.1.1 (2019-09-06)

- FEAT: support passing in credentials to main in L0Manager
- enh: updated link to ec2 catalog to use jsdelivr.net from github
- feat: l0manager.main now returns a promise (needed for isitfit-excel)


Version 0.1.0 (2019-09-06)

- ENH: testing cloudtrail lookupevents for runinstances and modifyinstanceattributes
- ENH: convert to OOP
- FEAT: integrate fetching cloudwatch metrics, merging them with the cloudtrail instance type
- feat: move l1manager into separate file
- feat: add number of ec2 machines, start date, and end date
