// var request = require('request');
var rp = require('request-promise');
var Promise = require('bluebird');

var CloudtrailManager = require('./CloudtrailManager.js');
const SECONDS_IN_ONE_DAY = 60*60*24; // # 86400  # used for granularity (daily)

// utility function
function add(a, b) {
    return a + b;
}


// Level 1 manager
// https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS#Creating_our_finished_constructor
function L1Manager(priorDate, today, cloudtrail, cloudwatch, ec2) {
	
	this.priorDate = priorDate;
	this.today = today;
	this.cloudtrail = cloudtrail;
	this.cloudwatch = cloudwatch;
	this.ec2 = ec2;
	this.dataCloudTrail = [];

	const self = this;
	
	// go through ec2 instances
	this.wrapEc2DescribeInstances = function(NextToken) {
		var params_ec2_desc = {
		  DryRun: false,
		  NextToken: NextToken
		};
		
		function handleEvent(err, data) {
			if (err) {
				console.log("Error", err.stack);
				return;
			}
			
			console.log("New Page of ec2.describeInstances");
			//console.log(data);
			var p_all = []
			data.Reservations.forEach(function(r) {
				r.Instances.forEach(function(x) {
					// append an artificial entry for this instance's "now" type in cloudtrail's data
					// console.log([x.InstanceId, x.InstanceType]);
					const newRow = {'EventTime': self.today, 'InstanceId': x.InstanceId, 'NewType': x.InstanceType};
					self.dataCloudTrail.push(newRow);
					
					// get the instance's metrics
					p_all.push(self.wrapCloudwatchMetrics(x.InstanceId));
				});	
			});

			// resolve sequentially all cloudwatch-metric-per-instance promises first, then proceed to next aws ec2-describe-instances page
			// http://stackoverflow.com/questions/29546898/ddg#33155268
			return Promise
				.map(p_all,
					function (promiseVal) {
					    return promiseVal;
					}, {concurrency: 1})
				.then(function() {
					// continue to next page
					if(data.NextToken) return self.wrapEc2DescribeInstances(data.NextToken);
					// done
					return null;
				});
		}
		
		// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/EC2.html#describeInstances-property
		function doit() {
			return self.ec2.describeInstances(params_ec2_desc).promise().then(function(data) { return handleEvent(null, data); });
		}
		return doit();
	};
	
	
	// Fetch cloudtrail history (since cannot be done 1 instance at a time)
	this.wrapCloudtrailLookupeventsAll = function () {
		const cm_run = new CloudtrailManager(self.cloudtrail, 'RunInstances', self.priorDate, self.today);
		var p2a = cm_run.pull().then(function() {
			self.dataCloudTrail = self.dataCloudTrail.concat(cm_run.data);
		});
		
		
		// FIXME not sure why the rest of this function executes without waiting for the pull call above
		// console.log("XXX")
		
		const cm_modified = new CloudtrailManager(cloudtrail, 'ModifyInstanceAttribute', self.priorDate, self.today);
		var p2b = cm_modified.pull().then(function() {
			self.dataCloudTrail = self.dataCloudTrail.concat(cm_modified.data);
		});
		
		return [p2a, p2b];
	};

	// gather totals
	this.total_billed = 0;
	this.total_used = 0;
	this.n_ec2 = 0;
	
	// for a single ec2 instance, get cloudwatch metrics
	this.wrapCloudwatchMetrics = function(instanceId, NextToken) {
		
		var params_cw_gms = {
		  StartTime: self.priorDate,
		  EndTime: self.today,
		  MetricName: 'CPUUtilization',
		  Namespace: 'AWS/EC2',
		  Period: SECONDS_IN_ONE_DAY,
		  Dimensions: [
		    {
		      Name: 'InstanceId',
		      Value: instanceId
		    },
		  ],
		  Statistics: [
		    'SampleCount',
		    'Average',
		  ],
		  Unit: 'Percent'
		};
		
		function handleEvent(err, data) {
			if (err) {
				console.log("Error", err.stack);
				return;
			}
			
			console.log("New Page of cw.getMetricStatistics");
			//console.log(data.Datapoints);
			
			// filter the cloudtrail array for this instance
			var sub1Cloudtrail = self.dataCloudTrail.filter(function(r) { return r.InstanceId==instanceId });
			//console.log("sub1cloudtrail", sub1Cloudtrail);
			
			// process
			data.Datapoints = data.Datapoints.map(function(r_metric) {
				// append number of hours
				r_metric.nhours = Math.ceil(r_metric.SampleCount/12);

				// distribute the cloudtrail subset of data into the metrics data
				var sub2Cloudtrail = sub1Cloudtrail.filter(function(r_cloudtrail) {
					return r_metric.Timestamp <= r_cloudtrail.EventTime;
				});
				//console.log
				if(sub2Cloudtrail.length==0) {
					r_metric.instanceType = null;
					r_metric.cost_hourly = null;
					r_metric.capacity_billed = null;
					r_metric.capacity_used = null;
					return r_metric;
				}
				
				// prepare for summary				
				r_metric.instanceType = sub2Cloudtrail[0].NewType;
				r_metric.cost_hourly = self.ec2_catalog[sub2Cloudtrail[0].NewType];
				r_metric.capacity_billed = r_metric.nhours*r_metric.cost_hourly;
				r_metric.capacity_used   = r_metric.nhours*r_metric.cost_hourly*r_metric.Average/100;
				
				return r_metric;
			});
			
			// get summary results
			const instance_billed = data.Datapoints.map(function(r_metric) {
					return r_metric.capacity_billed;
				}).reduce(add, 0);
			const instance_used = data.Datapoints.map(function(r_metric) {
					return r_metric.capacity_used;
				}).reduce(add, 0);
				
			// gather
			self.total_billed += instance_billed;
			self.total_used += instance_used;
			self.n_ec2++;


			// console.log("metrics merged with cloudtrail", data.Datapoints);
		}
		
		// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/CloudWatch.html#getMetricStatistics-property
		function doit() {
			return self.cloudwatch.getMetricStatistics(params_cw_gms).promise().then(function(data) { return handleEvent(null, data); });
		}
		return doit();
	};
	
	// this.ec2_catalog = [];
	this.ec2_catalog = {};
	this.getEc2Catalog = function() {
		console.log("Fetching EC2 catalog")
		// switching to jsdelivr.net (official CDN) trying to fix issue of CORS + cleaner to use aCDN
		//const URL = 'https://gitlab.com/autofitcloud/www.ec2instances.info-ec2op/raw/master/www.ec2instances.info/t3b_smaller_familyL2.json';
		const URL = 'https://cdn.jsdelivr.net/gh/autofitcloud/www.ec2instances.info-ec2op/www.ec2instances.info/t3b_smaller_familyL2.json';
		
		// https://stackoverflow.com/a/29853445/4126114
		/* await response(URL, function(error, response, body) {
			if (error || response.statusCode != 200) {
				console.error(error, response.statusCode)
				return;
			}
			
		    var importedJSON = JSON.parse(body);
		    ...
		*/

		var options = {
    		uri: URL,
    		json: true
		}
		return rp(options).then(function(importedJSON) {
			
		    //data is the JSON string
		    // console.log('ec2 catalog json data', body);
		    //return;
		    const i1 = importedJSON.columns.indexOf('API Name');
		    const i2 = importedJSON.columns.indexOf('Linux On Demand cost');
		    //var remapped = importedJSON.data.map(function(r) {
		    	// return {'API Name': r[i1], 'cost_hourly': r[i2]}
		    //});
		    importedJSON.data.forEach(function(r) {
		    	// make a map from instance type to hourly cost
		    	self.ec2_catalog[r[i1]] = r[i2];
		    });
		    
		    // console.log("example t2.large", self.ec2_catalog["t2.large"]);
		    // self.ec2_catalog = self.ec2_catalog.concat(remapped);
		});
	}
	
}


module.exports = L1Manager;