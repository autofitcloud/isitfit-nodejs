// Usage
// npm install
// node example.js

var Promise = require('bluebird');

var AWS = require('aws-sdk');

// https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/using-promises.html
AWS.config.setPromisesDependency(Promise);

var L1Manager = require('./L1Manager.js');

AWS.config.update({region: 'eu-central-1'});

//////////////////////////////////
function main(credentials) {
    // credentials - dict with keys: accessKeyId, secretAccessKey, region
    //               If it is skipped, then the ~/.aws/credentials file is checked
    //
    // Returns a promise with "data". Check cli.js for an example
    
    // aws sdk clients
    var ec2 = new AWS.EC2(credentials);
    var cloudtrail = new AWS.CloudTrail(credentials);
    var cloudwatch = new AWS.CloudWatch(credentials);

    // define start/end dates
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/CloudTrail.html#lookupEvents-property
    var today = new Date();
    var ndays = 90;
    var priorDate = new Date(new Date().setDate(today.getDate()-ndays));
    console.log("date range: ", priorDate, today);
    
    // start
    const l1man = new L1Manager(priorDate, today, cloudtrail, cloudwatch, ec2);
    var p1 = l1man.getEc2Catalog();
    var p2 = l1man.wrapCloudtrailLookupeventsAll();
    
    return Promise
        .all([p1, p2[0], p2[1]])
        .then(function() {
            return l1man.wrapEc2DescribeInstances();
        })
        .then(function() {
            var data = {
                priorDate: priorDate,
                today: today,
                n_ec2: l1man.n_ec2,
                total_billed: l1man.total_billed,
                total_used: l1man.total_used
            };
            return new Promise(function(resolve, reject) {
                return resolve(data);
            });
        });
}


module.exports = {
    main: main
}