var L0Manager = require('./L0Manager.js');
L0Manager.main().then(function(data) {
    console.log("Results");
    console.log("Start date: ", data.priorDate);
    console.log("End date: ", data.today);
    console.log("N ec2: ", data.n_ec2);
    console.log("Total billed: ", data.total_billed);
    console.log("Total used: ", data.total_used);
    console.log("CWAU: ", data.total_used/data.total_billed*100);
})