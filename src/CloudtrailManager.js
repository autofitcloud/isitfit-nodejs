function CloudtrailManager(cloudtrail, EventName, priorDate, today) {
	// console.log("got", NextToken)
	
	this.priorDate = priorDate;
	this.today = today;
	this.EventName = EventName;
	this.cloudtrail = cloudtrail;
	this.data = [];
	const self = this;
	
	function getType(rp_dict) {
		if(self.EventName=='RunInstances') {
			return rp_dict.instanceType;
		}
		
		if(self.EventName=='ModifyInstanceAttribute') {
		  	if('instanceType' in rp_dict) {
		  		return rp_dict.instanceType.value;
		  	}
		  	
	        if('attribute' in rp_dict) {
	          if(rp_dict['attribute']=='instanceType') {
	  			return rp_dict.value;
	          }
		  	}
		  	return null;
		}

		return null;
	}
	
	function getInstanceId(event, rp_dict) {
		if(self.EventName=='RunInstances') {
			return event.Resources.filter(function(x) { return x.ResourceType=='AWS::EC2::Instance' })[0].ResourceName;
		}
		
		if(self.EventName=='ModifyInstanceAttribute') {
		  	return rp_dict.instanceId;
		}

		return null;
	}
	
	function handleEvent(err, data) {
		  //console.log("enter cloudtrailmanager.handleevent")
		  if (err) {
		  	console.log(err, err.stack); // an error occurred
		  	return;
		  }
		  
		  //console.log(data);           // successful response
		  data.Events.forEach(function(event) {
		  	var ce_dict = JSON.parse(event.CloudTrailEvent);
		  	var rp_dict = ce_dict.requestParameters;
		  	var newType = getType(rp_dict);
		  	if(!newType) return;
		  	
		  	var instanceId = getInstanceId(event, rp_dict);
		  	
		  	//console.log(rp_dict)
		  	self.data.push({'EventTime': ce_dict.eventTime, 'InstanceId': instanceId, 'NewType': newType});
		  	//console.log(ce_dict.eventTime, instanceId, newType);
		  });
		  
		  //console.log("todo", data.NextToken);
		  if(data.NextToken) {
		  	self.params_ct_le.NextToken = data.NextToken;
		  	return innerCtLe();
		  }
		  
		  //console.log("exit cloudtrailmanager.handleevent")
		  return null;
	}
	
	function innerCtLe() {
		//console.log("enter cloudtrailmanager.innerctle")
		console.log("    Cloudtrail lookup-events new page. event name / next token = ", self.EventName, self.params_ct_le.NextToken);
		
		// .promise(); // .promise is executing this twice. Not sure why
		return self.cloudtrail.lookupEvents(self.params_ct_le).promise().then(function(data) { handleEvent(null, data); });
		//console.log("exit cloudtrailmanager.innerctle")
	}
	
	this.params_ct_le = {
	  StartTime: self.priorDate,
	  EndTime: self.today,
	  LookupAttributes: [
	    //{ AttributeKey: 'ReadOnly', AttributeValue: 'false' },
	    // is only 1 entry supported here too?
	    { AttributeKey: 'EventName', AttributeValue: self.EventName },
	  ],
	  MaxResults: 5,
	  NextToken: null
	};


	this.pull = function() {
		console.log("Cloudtrail lookup-events. pull. event name", self.EventName);
		self.params_ct_le.NextToken = null;
		return innerCtLe();
		//console.log("exit cloudtrailmanager.pull")
	};
	

}


module.exports = CloudtrailManager;