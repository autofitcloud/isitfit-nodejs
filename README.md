nodejs implementation behind the excel add-in

Usage

```
npm install
node src/cli.js
```

Bundle for browser

```
npm install --save-dev browserify
node_modules/browserify/bin/cmd.js src/L0Manager.js --standalone isitfit -o dist/isitfit.js
```
